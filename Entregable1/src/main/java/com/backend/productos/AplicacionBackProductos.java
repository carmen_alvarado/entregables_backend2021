package com.backend.productos;

import com.backend.productos.modelo.Producto;
import com.backend.productos.servicio.ServicioProducto;
import com.backend.productos.servicio.ServicioUsuario;
import com.backend.productos.servicio.impl.ServicioProductoImpl;
import com.backend.productos.servicio.impl.ServicioUsuarioImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AplicacionBackProductos {

	public static void main(String[] args) {
		SpringApplication.run(AplicacionBackProductos.class, args);
	}

	@Bean
	ServicioProducto servicioProducto() {
		return new ServicioProductoImpl();
	}

	@Bean
	ServicioUsuario servicioUsuario() {
		return new ServicioUsuarioImpl();
	}
}
