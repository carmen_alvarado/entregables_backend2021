package com.backend.productos.controlador;

import com.backend.productos.modelo.Producto;
import com.backend.productos.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Producto> obtenerProductos(){
        //return Arrays.asList(new Producto(1,"Pin 3","Pin con seguro, tamaño 3cm diam",75.00,100));
        return this.servicioProducto.obtenerProductos();
    }

    @GetMapping("/{idProducto}")
    public ResponseEntity obtieneProductoId(@PathVariable(name = "idProducto") long id){

        if(this.servicioProducto.obtenerProductoId(id) == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(this.servicioProducto.obtenerProductoId(id));
    }


    @PostMapping
    public ResponseEntity<String> agregarProducto(@RequestBody Producto p){
        this.servicioProducto.agregarProducto(p);
        return new ResponseEntity<>("Producto agregado satisfactoriamente.", HttpStatus.CREATED);
    }

    @PutMapping("/{idProducto}")
    public ResponseEntity sustituirProducto(@PathVariable(name = "idProducto") long id,
                                  @RequestBody Producto p){
       if( this.servicioProducto.obtenerProductoId(id) == null){
            return new ResponseEntity<>("El Producto no existe, imposible sustituir.", HttpStatus.NOT_FOUND);
        } else {
            this.servicioProducto.sustituirProducto(id, p);
            return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
        }
    }

    @DeleteMapping("/{idProducto}")
    public ResponseEntity eliminarProducto(@PathVariable(name = "idProducto") long id){
        if( this.servicioProducto.obtenerProductoId(id) == null) {
            return new ResponseEntity<>("Producto no encontrado, imposible eliminar.", HttpStatus.NOT_FOUND);
        } else {
            this.servicioProducto.eliminarProducto(id);
            //"Producto eliminado correctamente."
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PatchMapping("/{idProducto}")
    public ResponseEntity modificarPrecioProducto(@PathVariable(name = "idProducto") long id,
                                  @RequestBody Producto productoConPrecio){
        final Producto prodocutoConNuevoPrecio = this.servicioProducto.obtenerProductoId(id);
        if(prodocutoConNuevoPrecio == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if(productoConPrecio.getPrecio() > 0)
            prodocutoConNuevoPrecio.setPrecio(productoConPrecio.getPrecio());
        return new ResponseEntity<>(prodocutoConNuevoPrecio, HttpStatus.OK);

    }

}
