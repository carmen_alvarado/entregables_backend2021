package com.backend.productos.controlador;

import com.backend.productos.modelo.Producto;
import com.backend.productos.modelo.Usuario;
import com.backend.productos.servicio.ServicioProducto;
import com.backend.productos.servicio.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
//@RequestMapping("/productos/{idProducto}/usuarios")
public class ControladorProductoUsuario {

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    ServicioUsuario servicioUsuario;

    @GetMapping("/productos/{idProducto}/usuarios")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable long idProducto) {
        //return this.servicioProducto.obtenerProductoId(idProducto).getIdsUsuarios();
        Producto pr = this.servicioProducto.obtenerProductoId(idProducto);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getIdsUsuarios()!=null)
            return ResponseEntity.ok(pr.getIdsUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /*@GetMapping("/{indiceUsuario}")
    public Usuario obtenerUsuariosProducto(@PathVariable long idProducto,
                                           @PathVariable int indiceUsuario) {
        final long idUsuario = this.servicioProducto
                .obtenerProductoId(idProducto)
                .getIdsUsuarios().get(indiceUsuario);
        return this.servicioUsuario.obtenerUsuario(idUsuario);
    }*/

    static class UsuarioProducto {
        public int idUsuario;
    }

    @PostMapping("/productos/{idProducto}/usuarios")
    public void agregarUsuarioProducto(@PathVariable long idProducto,
                                       @RequestBody UsuarioProducto usu) {
        try {
            this.servicioUsuario.obtenerUsuario(usu.idUsuario);
            this.servicioProducto.obtenerProductoId(idProducto)
                    .getIdsUsuarios().add(usu.idUsuario);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


}
