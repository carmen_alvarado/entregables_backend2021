package com.backend.productos.controlador;

import com.backend.productos.modelo.Usuario;
import com.backend.productos.servicio.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class ControladorUsuario {

    @Autowired
    ServicioUsuario servicioUsuario; // Inyección de Dependencias

    @GetMapping
    public List<Usuario> obtenerUsuarios() {
        //return Arrays.asList(new Usuario(1L,"Zapatillas Locas",5000.00, 300));
        return this.servicioUsuario.obtenerUsuarios();
    }

    @GetMapping("/{idUsuario}")
    public Usuario obtenerUsuarioPorId(@PathVariable(name = "idUsuario") int id) {
        return this.servicioUsuario.obtenerUsuario(id);

    }

    @PostMapping
    public void crearUsuario(@RequestBody Usuario p) {

        this.servicioUsuario.agregarUsuario(p);
    }
}