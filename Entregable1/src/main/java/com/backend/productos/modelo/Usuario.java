package com.backend.productos.modelo;

public class Usuario {

    long id;
    String login;
    String nombre;

    public Usuario(long id, String login, String nombre) {
        this.id = id;
        this.login = login;
        this.nombre = nombre;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
