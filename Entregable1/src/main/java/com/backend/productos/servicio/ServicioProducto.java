package com.backend.productos.servicio;

import com.backend.productos.modelo.Producto;

import java.util.List;

public interface ServicioProducto {

    //C Create
    public long agregarProducto(Producto p);

    //R Read, nos devolverá todos los productos
    public List<Producto> obtenerProductos();

    //R Reade, nos devolverá solo un Producto identificado por su id
    public Producto obtenerProductoId(long id);

    //U Update, actualizaremos un Producto, alguna de las características del mismo Producto p
    // identificado por su id
    public Producto sustituirProducto(long id,Producto p);

    //D Delete, eliminaremos alguno de nuestros Productos por el id del mismo
    public void eliminarProducto(long id);

}
