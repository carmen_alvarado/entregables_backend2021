package com.backend.productos.servicio;

import com.backend.productos.modelo.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ServicioUsuario {

    public long agregarUsuario(Usuario u);

    public List<Usuario> obtenerUsuarios();

    public Usuario obtenerUsuario(long id);

}
