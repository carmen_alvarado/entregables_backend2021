package com.backend.productos.servicio.impl;

import com.backend.productos.modelo.Producto;
import com.backend.productos.servicio.ServicioProducto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


public class ServicioProductoImpl implements ServicioProducto {

    //Para que al agregar el Producto el id sea secuencial, se inicia en 0 y cuando
    //le mando llamar por primera vez tendrá un 1, la segunda 2.
    private final AtomicLong consecutivoIds = new AtomicLong(0L);
    private final List<Producto> productos = new ArrayList<>();

    @Override
    public long agregarProducto(Producto p) {
        //Con consecutivosIds colocamos el id a los productos que agreguemos
        p.setId(consecutivoIds.incrementAndGet());
        //Agregamos el producto a la lista
        productos.add(p);
        //Regresamos el id del producto recién agregado
        return p.getId();
    }

    @Override
    public List<Producto> obtenerProductos() {
        return Collections.unmodifiableList(this.productos);
    }

    @Override
    public Producto obtenerProductoId(long id) {
        for(Producto p: this.productos) {
            if(p.getId() == id)
                return p;

        }
        return null;
        //throw new RuntimeException("No existe el producto con tal id. ".concat(String.valueOf(id)));
    }

    @Override
    public Producto sustituirProducto(long id, Producto p) throws IndexOutOfBoundsException{
        for(int i = 0; i < this.productos.size() ; ++i){
            if(this.productos.get(i).getId() == id) {
                //Sustituimos todo el producto, exceptuando el id
                p.setId(id);
                this.productos.set(i,p);
                return p;
            }
        }
        return null;//throw new RuntimeException("No es posible reemplazar el producto, no existe alguno con tal id.".concat(String.valueOf(id)));
    }

    @Override
    public void eliminarProducto(long id) {
        for(Producto p: this.productos) {
            if(p.getId() == id) {
                this.productos.remove(p);
                break;
            }
        }
    }


}
