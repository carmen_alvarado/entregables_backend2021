package com.backend.productos.servicio.impl;

import com.backend.productos.modelo.Usuario;
import com.backend.productos.servicio.ServicioUsuario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


public class ServicioUsuarioImpl implements ServicioUsuario {

   private final AtomicLong secuenciaIds = new AtomicLong(0L);
   private final List<Usuario> usuarios = new ArrayList<>();

    @Override
    public long agregarUsuario(Usuario u) {
        u.setId(secuenciaIds.incrementAndGet());
        usuarios.add(u);
        return u.getId();
    }

    @Override
    public List<Usuario> obtenerUsuarios() {
        return Collections.unmodifiableList(this.usuarios);
    }

    @Override
    public Usuario obtenerUsuario(long id) {
        for (Usuario u : this.usuarios) {
            if (u.getId() == id)
                return u;
        }
        return null;
        //throw new RuntimeException("No existe el Usuario con tal id. ".concat(String.valueOf(id)));
    }


}
