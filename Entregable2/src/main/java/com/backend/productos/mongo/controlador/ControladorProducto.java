package com.backend.productos.mongo.controlador;

import com.backend.productos.mongo.modelo.Producto;
import com.backend.productos.mongo.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Producto> obetenerProductos(){
        return this.servicioProducto.obtenerTodos();
    }

    @GetMapping("/{id}")
    public Producto obtenerProductoPorId(@PathVariable String id){
        return  this.servicioProducto.buscarPorId(id);
    }

    @PostMapping
    public void agregarProducto(@RequestBody Producto productoNuevo){
        productoNuevo.setId(null);
        this.servicioProducto.agregar(productoNuevo);
    }

    @PutMapping
    public void putProductos(@RequestBody Producto productoAActualizar){
        servicioProducto.sustituirProducto(productoAActualizar);
    }

    @DeleteMapping
    public boolean borrarProducto(@RequestBody Producto productoABorrar){
        return this.servicioProducto.borrarProducto(productoABorrar);
    }

}
