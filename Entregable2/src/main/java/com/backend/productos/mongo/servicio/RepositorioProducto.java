package com.backend.productos.mongo.servicio;

import com.backend.productos.mongo.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String> {

}
