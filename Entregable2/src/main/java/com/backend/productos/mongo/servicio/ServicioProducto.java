package com.backend.productos.mongo.servicio;

import com.backend.productos.mongo.modelo.Producto;

import java.util.List;

public interface ServicioProducto {
    public void agregar(Producto p);
    public Producto buscarPorId(String id);
    public List<Producto> obtenerTodos();
    public Producto sustituirProducto(Producto p);
    public boolean borrarProducto(Producto p);
}
