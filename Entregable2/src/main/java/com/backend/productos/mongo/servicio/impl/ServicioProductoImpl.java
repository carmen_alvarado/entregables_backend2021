package com.backend.productos.mongo.servicio.impl;

import com.backend.productos.mongo.modelo.Producto;
import com.backend.productos.mongo.servicio.RepositorioProducto;
import com.backend.productos.mongo.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProducto repositorioProducto;

    @Override
    public void agregar(Producto p) {
        this.repositorioProducto.insert(p);
    }

    @Override
    public Producto buscarPorId(String id) {
        Optional<Producto> resultado =this.repositorioProducto.findById(id);
        return resultado.isPresent() ? resultado.get() : null;
    }

    @Override
    public List<Producto> obtenerTodos() {
        return this.repositorioProducto.findAll();
    }

    @Override
    public Producto sustituirProducto(Producto productoASuistituir) {
        return this.repositorioProducto.save(productoASuistituir);
    }

    @Override
    public boolean borrarProducto(Producto productoABorrar) {
        try {
            repositorioProducto.delete(productoABorrar);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
